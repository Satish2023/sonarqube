from geopy.geocoders import Nominatim
import csv

def reverse_geocode(latitude, longitude):
    geolocator = Nominatim(user_agent="reverse_geocoding_app")
    location = geolocator.reverse((latitude, longitude), exactly_one=True)
    if location:
        return location

    return None

if __name__ == "__main__":
    latitude = 25.255555  # Replace with your latitude
    longitude = 83.117373  # Replace with your longitude

    location = reverse_geocode(latitude, longitude)
    
    if location:
        address = location.address
        address_parts = address.split(', ')
        
        # Extract the individual components
        district = address_parts[-5]
        town = address_parts[-4]
        state = address_parts[-3]
        country = address_parts[-2]

        # Write the result to a CSV file
        with open("reverse_geocoded.csv", "w", newline="") as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerow(["Latitude", "Longitude", "District", "Town", "State", "Country"])
            csv_writer.writerow([latitude, longitude, district, town, state, country])
        
        print(f"Reverse geocoded address written to reverse_geocoded.csv")
    else:
        print("Location not found.")
