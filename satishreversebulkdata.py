from geopy.geocoders import Nominatim
import csv

def reverse_geocode(latitude, longitude):
    geolocator = Nominatim(user_agent="reverse_geocoding_app")
    location = geolocator.reverse((latitude, longitude), exactly_one=True)
    if location:
        return location
    return None

if __name__ == "__main__":
    # Read input from input.csv file containing Latitude and Longitude columns
    with open(r"E:\satishcode.csv", "r") as input_file:
        csv_reader = csv.reader(input_file)
        next(csv_reader)  # Skip header row if it exists
        rows = list(csv_reader)

    results = []
    
    for row in rows:
        latitude, longitude = float(row[0]), float(row[1])
        location = reverse_geocode(latitude, longitude)
        if location:
            address = location.address
            address_parts = address.split(', ')
            
            # Check the length of address_parts before accessing its elements
            if len(address_parts) >= 5:
                district = address_parts[-5]
                town = address_parts[-4]
                state = address_parts[-3]
                country = address_parts[-2]
                results.append([latitude, longitude, district, town, state, country])
    
    # Write the results to output.csv file
    with open(r"D:\Coding course\sonarqube-1\reverse_geocoded_output.csv", "w", newline="") as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(["Latitude", "Longitude", "District", "Town", "State", "Country"])
        csv_writer.writerows(results)
    
    print(f"Reverse geocoded addresses written to reverse_geocoded_output.csv")
