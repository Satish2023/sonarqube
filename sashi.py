from geopy.geocoders import Nominatim

def reverse_geocode(latitude, longitude):
    geolocator = Nominatim(user_agent="reverse_geocoding_app")
    location = geolocator.reverse((latitude, longitude), exactly_one=True)
    if location:
        return location.address
    else:
        return "Location not found."

if __name__ == "__main__":
    latitude = 25.255555  # Replace with your latitude
    longitude = 83.117373  # Replace with your longitude

    address = reverse_geocode(latitude, longitude)
    print(address)