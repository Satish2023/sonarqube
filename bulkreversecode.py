from geopy.geocoders import Nominatim
import csv

def reverse_geocode(latitude, longitude):
    geolocator = Nominatim(user_agent="reverse_geocoding_app")
    location = geolocator.reverse((latitude, longitude), exactly_one=True)
    if location:
        return location
    return None

if __name__ == "__main__":
    # Define a list of coordinates (latitude, longitude)
    coordinates = [
        (25.255555, 83.117373),
        (34.052235, -118.243683),
        # Add more coordinates as needed
    ]

    # Create a CSV file for writing
    with open("bulk_reverse_geocoded.csv", "w", newline="") as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(["Latitude", "Longitude", "District", "Town", "State", "Country"])

        for latitude, longitude in coordinates:
            location = reverse_geocode(latitude, longitude)
            if location:
                address = location.address
                address_parts = address.split(', ')
                district = address_parts[-5]
                town = address_parts[-4]
                state = address_parts[-3]
                country = address_parts[-2]

                # Write the results to the CSV file for each coordinate
                csv_writer.writerow([latitude, longitude, district, town, state, country])
                print(f"Reverse geocoded for {latitude}, {longitude} written to bulk_reverse_geocoded.csv")
            else:
                print(f"Location not found for {latitude}, {longitude}")
    
    print("Bulk reverse geocoding completed.")
