import http.server
import socketserver

# Define the HTML content you want to display
html_content = """
<DOCTYPE html>
<html>
<head>
    <title>Python HTML App</title>
</head>
<body>
    <h1>Hello, Billu gaandu h  aur gandu rah jaayega</h1>
   
</body>
</html>
"""

# Create a Python HTTP server to serve the HTML content
class MyHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(html_content.encode())

# Set up the server
PORT = 8000
with socketserver.TCPServer(("", PORT), MyHandler) as httpd:
    print(f"Serving at http://localhost:{PORT}")
    httpd.serve_forever()
